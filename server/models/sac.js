'use strict';

module.exports = function (Sac) {
    Sac.observe("after save", (ctx, next) => {
        // console.log(ctx);
        Sac.app.models.Email.send({
            to: 'knaimero@gmail.com',
            from: 'web@condor.com.br',
            subject: 'SAC do Site Condor',
            html: `
                <h1 style="font-weight: 700; border-bottom: 1px solid #000; padding-bottom: 1rem; margin-bottom: 2.5rem">Assunto: ${ctx.instance.assunto}</h1>
                <h2 style="text-align: center;">Loja que quer contatar: <br>${ctx.instance.loja}</h2>
                <h2 style="font-weight: 700; border-bottom: 1px solid #000; padding-bottom: 1rem; margin-bottom: 2rem">Dados básicos</h2>
                <p><strong>Nome:</strong> ${ctx.instance.nome}</p>
                <p><strong>E-mail:</strong> ${ctx.instance.email}</p>
                <p><strong>CPF:</strong> ${ctx.instance.cpf}</p>
                <p><strong>Telefone:</strong> ${ctx.instance.telefone}</p>
                <p><strong>Data de Nascimento:</strong> ${ctx.instance.data_nascimento}</p>
                <p><strong>CEP:</strong> ${ctx.instance.cep}</p>
                <p><strong>Rua:</strong> ${ctx.instance.rua}</p>
                <p><strong>Complemento:</strong> ${ctx.instance.complemento}</p>
                <p><strong>Bairro:</strong> ${ctx.instance.bairro}</p>
                <p><strong>Cidade:</strong> ${ctx.instance.cid}</p>
                <p><strong>UF:</strong> ${ctx.instance.uf}</p>
                <h2 style="font-weight: 700; border-bottom: 1px solid #000; padding-bottom: 1rem; margin-bottom: 2rem">Mensagem</h2>
                <p>${ctx.instance.mensagem}</p>

            `
        }, (err, mail) => {
            console.log(mail);
            console.log('Err =>', err);
        });
        next;
    })
};
