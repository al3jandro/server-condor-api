"use strict";

module.exports = function(Produtos) {
  Produtos.findProduto = function(cod_produto, cb) {
    Produtos.find({
      where: {cod_produto: cod_produto}
    }, function(err, result) {
      cb(null, result);
    })
  };

  Produtos.remoteMethod("findProduto", {
    accepts: {
      arg: "cod_produto",
      type: "number"
    },
    returns: { arg: 'results', type: "string" },
    http: { path: '/findProduto', verb: 'get'}
  });

  Produtos.findCampanha = function(code, cb) {
    Produtos.find({
      where: {cod_campanha: code}
    }, function(err, result) {
      cb(null, result);
    })
  };

  Produtos.remoteMethod("findCampanha", {
    accepts: {
      arg: "code",
      type: "number"
    },
    returns: { arg: "results", type: "string" },
    http: { path: '/findCampanha', verb: 'get'}
  });


  Produtos.findDepartamento = function(code, cb) {
    Produtos.find({
      where: {
        departamento: code
      }
    }, function(err, result) {
      cb(null, result);
    })
  };

  Produtos.remoteMethod("findDepartamento", {
    accepts: {
      arg: "code",
      type: "number"
    },
    returns: { arg: "results", type: "string" },
    http: { path: '/findDepartamento', verb: 'get'}
  });

  Produtos.findSetor = function(depart, setor,cb) {
    Produtos.find({
      where: {
        and: [{ departamento: depart}, { setor: setor}]
      }
    }, function(err, result) {
      cb(null, result);
    })
  };

  Produtos.remoteMethod("findSetor", {
    accepts: [
      { arg: "departamento", type: "number" },
      { arg: "setor", type: "number" }
    ],
    returns: { arg: "results", type: "string" },
    http: { path: '/findSetor', verb: 'get'}
  });

  Produtos.findSlug = function(slug,cb) {
    Produtos.findProduto({
      where: {
        and: { slug: slug}
      }
    }, function(err, result) {
      cb(null, result);
    })
  };

  Produtos.remoteMethod("findSlug", {
    accepts: { arg: "slug", type: "string" },
    returns: { arg: "results", type: "string" },
    http: { path: '/findSlug', verb: 'get'}
  });

  Produtos.search = function(code, cb) {
    Produtos.find({
      where: {
        dsc_produto: {
          like: code,
          options: "i"
        }
      }
    }, function(err, result) {
      cb(null, result);
    })
  };

  Produtos.remoteMethod("search", {
    accepts: {
      arg: "search",
      type: "string"
    },
    returns: { arg: "results", type: "string" },
    http: { path: '', verb: 'get'}
  });

  Produtos.updateKit = function(id, kit, cb) {
    Produtos.findById(id, function (err, product){
      product.dsc_kit = kit;
      return product.save(function (err, result){
        cb(null, result);
      })
    })
  }

  Produtos.remoteMethod("updateKit", {
    accepts: [
      { arg: "id", type: "number", required: true },
      { arg: "kit", type: "string", required: true }
    ],
    returns: { arg: "results", type: "string" },
    http: { path: '', verb: 'post'}
  });

  Produtos.menuDepartamento = function(cb) {
    var ds = Produtos.dataSource;
    var sql = `SELECT * FROM condor_api.departamento;`;
    ds.connector.query(sql, null, (err, products) => {
      cb(err, products);
    });
  }

  Produtos.remoteMethod("menuDepartamento", {
    returns: { arg: "data", type: ['Produtos'], root: true },
    description: 'Get list departamento',
    http: { path: '', verb: 'get'}
  });

  Produtos.menuSetor = function(cb) {
    var ds = Produtos.dataSource;
    var sql = `SELECT * FROM condor_api.setor;`;
    ds.connector.query(sql, null, (err, products) => {
      cb(err, products);
    });
  }

  Produtos.remoteMethod("menuSetor", {
    returns: { arg: "data", type: ['Produtos'], root: true },
    description: 'Get list setor',
    http: { path: '', verb: 'get'}
  });

}
