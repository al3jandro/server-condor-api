var server = require('../server');
var ds = server.dataSources.mysqlDB;
var lbTables = ['Campanha', 'Produtos', 'Customer'];

ds.autoupdate(
 lbTables,
 function (er) {
   if (er) throw er;
   console.log(` Migrando a tabelas [${lbTables}] criadas em ${ds.adapter.name} `);
   ds.disconnect();
 }
);
